﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
//using UnityEditor.iOS.Xcode;
using UnityEditor.Callbacks;

class JenkinsAutoBuild
{
    static string[] SCENES = FindEnabledEditorScenes();

    static string APP_NAME = "KarenWazen";

    [MenuItem("Custom/CI/Build Android")]
    static void PerformAndroidBuild()
    {
        string version = CommandLineReader.GetCustomArgument("version");

        if (string.IsNullOrEmpty(version))
        {
            version = "1";
        }

        PlayerSettings.bundleVersion = "1.0." + version;
        PlayerSettings.Android.bundleVersionCode = int.Parse(version);

        string keystore = Directory.GetCurrentDirectory() + "/KarenWazenKeystore.keystore";
        PlayerSettings.Android.keystoreName = keystore;
        PlayerSettings.Android.keystorePass = "DvynRBjfi3T91rdh";
        PlayerSettings.Android.keyaliasName = "KarenWazen";
        PlayerSettings.Android.keyaliasPass = "gYXy6gDGxG6Zvqso";

        string target_dir = APP_NAME + ".apk";
        GenericBuild(SCENES, target_dir, BuildTargetGroup.Android, BuildTarget.Android, BuildOptions.CompressWithLz4HC);
    }

    [MenuItem("Custom/CI/Build Android Bundle")]
    static void PerformAndroidBundleBuild()
    {
        string version = CommandLineReader.GetCustomArgument("version");
        if (string.IsNullOrEmpty(version))
        {
            version = "1";
        }
        PlayerSettings.bundleVersion = "1.0." + version;
        PlayerSettings.Android.bundleVersionCode = int.Parse(version);
        string keystore = Directory.GetCurrentDirectory() + "/KarenWazenKeystore.keystore";
        PlayerSettings.Android.keystoreName = keystore;
        PlayerSettings.Android.keystorePass = "DvynRBjfi3T91rdh";
        PlayerSettings.Android.keyaliasName = "KarenWazen";
        PlayerSettings.Android.keyaliasPass = "gYXy6gDGxG6Zvqso";

        PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
        AndroidArchitecture aac = AndroidArchitecture.ARM64 | AndroidArchitecture.ARMv7;

        PlayerSettings.Android.targetArchitectures = aac;
        EditorUserBuildSettings.buildAppBundle = true;

        string target_dir = APP_NAME + ".aab";
        GenericBuild(SCENES, target_dir, BuildTargetGroup.Android, BuildTarget.Android, BuildOptions.CompressWithLz4HC);
    }

    [MenuItem("Custom/CI/Build iOS")]
    static void PerformiOSBuild()
    {
        string version = CommandLineReader.GetCustomArgument("version");

        if (string.IsNullOrEmpty(version))
        {
            version = "1";
        }

        PlayerSettings.bundleVersion = "1.0." + version;
        PlayerSettings.iOS.buildNumber = version;

        string target_dir = "iOS";
        GenericBuild(SCENES, target_dir, BuildTargetGroup.iOS, BuildTarget.iOS, BuildOptions.CompressWithLz4HC);
    }

    private static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled)
            {
                continue;
            }
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    [MenuItem("Custom/CI/Build Windows")]
    static void PerformWindowsBuild()
    {
        string target_dir = "Windows/" + APP_NAME + ".exe";
        GenericBuild(SCENES, target_dir, BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows, BuildOptions.None);
    }

    static void GenericBuild(string[] scenes, string target_dir, BuildTargetGroup build_group, BuildTarget build_target, BuildOptions build_options)
    {
        // Change logos
        if (UnityEditorInternal.InternalEditorUtility.HasPro())
        {
            PlayerSettings.SplashScreen.showUnityLogo = false;
            //var logos = new PlayerSettings.SplashScreenLogo[2];
            //var logo = (Sprite) AssetDatabase.LoadAssetAtPath("Assets/Textures/Title.png", typeof(Sprite));
            //logos[0] = PlayerSettings.SplashScreenLogo.Create(2f, logo);
            //PlayerSettings.SplashScreen.overlayOpacity = 0f;
            //PlayerSettings.SplashScreen.logos = logos;
            //PlayerSettings.SplashScreen.unityLogoStyle = PlayerSettings.SplashScreen.UnityLogoStyle.DarkOnLight;
            PlayerSettings.SplashScreen.backgroundColor = Color.black;
        }

        var buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = scenes,
            target = build_target,
            locationPathName = target_dir,
            options = build_options//BuildOptions.Development | BuildOptions.AcceptExternalModificationsToPlayer
        };
        EditorUserBuildSettings.SwitchActiveBuildTarget(build_group, build_target);

        var res = BuildPipeline.BuildPlayer(buildPlayerOptions).ToString();
        if (res != "New Report (UnityEngine.BuildReport)")
        {
            throw new Exception("BuildPlayer failure: " + res);
        }
    }

    /*
    [PostProcessBuild]
    public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            // Get plist
            string plistPath = pathToBuiltProject + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
            PlistElementDict rootDict = plist.root;

            // Set plist strings
            var buildKey = "NSLocationWhenInUseUsageDescription";
            rootDict.SetString(buildKey, "GPS location is required to use virtual guide");

            buildKey = "NSLocationAlwaysAndWhenInUseUsageDescription";
            rootDict.SetString(buildKey, "GPS location is required to use virtual guide");

            buildKey = "NSLocationAlwaysUsageDescription";
            rootDict.SetString(buildKey, "GPS location is required to use virtual guide");

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());
        }
    }
    */
}