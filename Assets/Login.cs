﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using BestHTTP;
using System;
using TMPro;

public class Token
{
    public string access_token;
    public string refresh_token;
    public string id_token;
    public string token_type;
    public int expires_in;
}

public class Login : MonoBehaviour
{
    [SerializeField] private string domain;
    [SerializeField] private string webScheme; 
    [SerializeField] private string redirectURI;
    [SerializeField] private string clientId1;
    [SerializeField] private string clientId2;
    [SerializeField] private string clientSecret;

    [SerializeField] private TMP_Text tokenText;
    [SerializeField] private UniWebView uniWebView;

    private Token token;

    private void Start()
    {
        uniWebView.AddUrlScheme(webScheme);
        uniWebView.OnMessageReceived += (view, message) => {
            if (message.Path.Equals("auth/login"))
            {
                var code = message.Args["code"];
                SendRequest(code);
                Destroy(uniWebView);
            }
        };
    }

    private void SendRequest(string code)
    {
        byte[] loginBytes = System.Text.Encoding.UTF8.GetBytes($"{clientId2}:{clientSecret}");
        string loginEncoded = System.Convert.ToBase64String(loginBytes);

        HTTPRequest request = new HTTPRequest(new Uri($"{domain}/oauth2/token"), HTTPMethods.Post, OnRequestFinished);
        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        request.AddHeader("Authorization", $"Basic {loginEncoded}");
        request.AddField("grant_type", "authorization_code");
        request.AddField("code", code);
        request.AddField("redirect_uri", redirectURI);
        request.Send();
    }

    private void OnRequestFinished(HTTPRequest originalRequest, HTTPResponse response)
    {
        string result = response.DataAsText;
        if (!result.Contains("error"))
        {
            token = JsonUtility.FromJson<Token>(result);
            tokenText.text = "<b>Token received:</b>\n" + result;
        }
        else
        {
            tokenText.text = "<b>Error:</b>\n" + result;
        }
    }

    public void LogIn()
    {
        Application.OpenURL($"{domain}/login?client_id={clientId1}&response_type=code&redirect_uri={redirectURI}");
        //uniWebView.Frame = new Rect(0, 0, Screen.width, Screen.height);
        //uniWebView.Load($"{domain}/login?client_id={clientId1}&response_type=code&redirect_uri={redirectURI}");
        //uniWebView.Show(false, UniWebViewTransitionEdge.Bottom, 0.2f);
    }
}
